﻿(function () {
    'use strict';

    angular.module('funcef-wizard.controller')
        .controller('NgfWizard', NgfWizard);

    NgfWizard.$inject = ['$scope', '$element', '$compile'];

    /* @ngInject */
    function NgfWizard($scope, $element, $compile) {
        var vm = this;
        vm.ctrl = $element.controller();

        vm.options = {
            classActive: 'active',
            classValid: 'valid',
            classInvalid: 'invalid'
        };

        init();

        ///////////

        function init() {
            addClassWizard();
            addEvents();
            addClassActive();
            createHeaderMobile();
        }

        function addEvents() {
            $scope.$watch('passo', function (newValue, oldValue) {
                    validStep(newValue, oldValue);
            });
        }

        function validStep(newValue, oldValue) {
            if (newValue > oldValue) {
                var validate = getStepValidate(oldValue);
                var functionVal = vm.ctrl[validate];

                if (functionVal && !functionVal()) {
                    $scope.passo = oldValue;
                } else {
                    addClassValid(oldValue);
                }
            } else {
                removeClassValid()
            }

            removeClassActive();
            addClassActive();
            setHeaderMobile();
        }

        function createHeaderMobile() {
            var html = '<h2 class="wz-h visible-xs-block">{{vm.wizardTitle}}</h2><p class="wz-p visible-xs-block">{{vm.wizardDescription}}</p>';
            $element.append($compile(html)($scope));
        }

        function setHeaderMobile() {
            var element = angular.element('li.'+ vm.options.classActive + ' a', $element);
            vm.wizardTitle = angular.element('label', element).text();
            vm.wizardDescription = angular.element('p', element).text();
        }

        function addClassActive() {
            getStep($scope.passo).addClass(vm.options.classActive);

        }

        function addClassValid(step) {
            getStep(step).addClass(vm.options.classValid);
        }

        function removeClassActive() {
            var steps = angular.element('li', $element);
            steps.removeClass(vm.options.classActive);
        }

        function removeClassValid() {
            getStep($scope.passo).removeClass(vm.options.classValid);
        }


        function addClassWizard() {
            $element.addClass('ngf-wizard2');
        }

        function getStep(step) {
            return angular.element('li:eq(' + (step - 1) + ')', $element);
        }

        function getStepValidate(step) {
            var link = $element.find('li:eq(' + (step - 1) + ') a');
            var validacao = link.attr('invalid');
            return validacao ? validacao.replace('vm.', '').replace('()', '') : '';
        }

    }
}());