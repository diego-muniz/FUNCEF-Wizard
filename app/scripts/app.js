﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name wizard
    * @version 1.0.0
    * @Componente Wizard para validação de passos
    */
    angular.module('funcef-wizard.controller', []);
    angular.module('funcef-wizard.directive', []);

    angular
    .module('funcef-wizard', [
      'funcef-wizard.controller',
      'funcef-wizard.directive'
    ]);
})();