﻿(function () {
    'use strict';

    angular
      .module('funcef-wizard.directive')
      .directive('ngfWizard', ngfWizard);

    ngfWizard.$inject = ['$timeout'];

    /* @ngInject */
    function ngfWizard($timeout) {
        return {
            restrict: 'A',
            controller: 'NgfWizard',
            controllerAs: 'vm',
            scope: {
                passo: '='
            }
        };
    }
}());