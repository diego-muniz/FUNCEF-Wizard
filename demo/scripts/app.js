﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name FuncefDemo
    * @version 1.0.0
    * @Componente para teste de componentes
    */
    angular.module('funcef-demo.controller', []);
    angular.module('funcef-mensagem.service', []);
    


    angular
        .module('funcef-demo', [
            'funcef-demo.controller',
            'ui.router',
            'funcef-wizard',
            'funcef-validate'
        ]);
})();