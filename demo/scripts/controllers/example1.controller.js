﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = [];

    /* @ngInject */
    function Example1Controller() {
        var vm = this;
        vm.proximoPasso = proximoPasso;
        vm.passoAnterior = passoAnterior;
        vm.formValid = formValid;
        init();
        
        function init(){
        	vm.passo = 1;
        }

        function proximoPasso(){
        	   vm.passo++;

        }

        function passoAnterior() {
            vm.passo--;
        }

        function formValid(){
        	if(vm.Campo){
        		return true;
        	}
        }
        

        



    }
}());